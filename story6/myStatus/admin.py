from django.contrib import admin
from django.urls import path
from . import views
from myStatus.models import Status

# Register your models here.
admin.site.register(Status)