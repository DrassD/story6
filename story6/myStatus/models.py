from django.db import models
from datetime import datetime

# Create your models here.
class Status(models.Model):
	state = models.TextField(max_length = 300)
	date = models.DateTimeField(default = datetime.now)