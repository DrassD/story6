from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Status
from .forms import StatusForm
from datetime import datetime

# Create your views here.
def status(request):
    displayState = Status.objects.all().values().order_by('-date')
  
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            displayState = Status()
            displayState.state = form.cleaned_data['your_status']
            displayState.date = datetime.now()
            displayState.save()
            return HttpResponseRedirect('/status')
    else:
        form = StatusForm()

    response = {
        'form' : form,
        'displayState' : displayState,
    }
    return render(request, 'status.html', response)
 
def delete(request):
    displayState = Status.objects.all()
    for item in displayState:
        item.delete()
    return redirect('/status')