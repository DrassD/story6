from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone
from .forms import StatusForm
from .models import Status
from .views import status
from selenium import webdriver
import time

# Create your tests here.
class StatusTestCase(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)
    def test_status_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, status)
    def template_is_used(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'status.html')
    def test_status_object_to_database(self):
        Status.objects.create(state='Kami', date=timezone.now())
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)

class NewVisitorTest(TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()
    def tearDown(self):
        self.browser.quit()
    def test_submit_coba_coba(self):
        self.browser.get('http://127.0.0.1:8000/status/')
        time.sleep(5)
        search_box = self.browser.find_element_by_name('your_status')
        search_box.send_keys('Coba Coba')
        search_box.submit()
        time.sleep(10)
    def test_title_is_correct(self):
        browser = self.browser.get('http://127.0.0.1:8000/status/')
        self.assertIn('Status', self.browser.title)